#/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd "$SCRIPT_DIR"

cp ../icns/vvvideo.icns  ../bin/vvvideo.app/Contents/Resources/

cd "$SCRIPT_DIR"
cd ..
plutil -insert  CFBundleIconFile -string vvvideo.icns bin/vvvideo.app/Contents/Info.plist


