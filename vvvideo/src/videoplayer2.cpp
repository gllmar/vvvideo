//
//  vplay.cpp
//  vvhap
//
//  Created by gllm on 2022-02-28.
//

#include "videoplayer2.hpp"

void vplay2::setup(int index)
{

    params_player.setName("vplay_"+to_string(index));
    params_player.add(enable.set("enable",1));
    params_player.add(disable_tick.set("disable_tick",0));
    params_player.add(direct_drive_b.set("direct_drive_b",0));
    params_player.add(direct_drive_f.set("direct_drive_f",0,-1,1));
    direct_drive_f.addListener(this, &vplay2::direct_drive_f_changed);
    params_player.add(player_rate_range.set("rate_range",1,0.1,64));
    player_rate_range.addListener(this, &vplay2::player_rate_range_changed);
    params_player.add(player_rate_multiplicator.set("rate_multiplicator",2, 0.1, 64));
    params_player.add(computed_rate.set("computed_rate",0,-10,10));
    params_player.add(backward.set("backward",0));
    
    params_player.add(player_a_load_movie_b.set("player_a_load",0));
    params_player.add(player_a_file.set("player_a_file",""));
    params_player.add(player_a_path.set("player_a_path",""));
    params_player.add(player_a_frame_position.set("player_a_frame",0,0,1));
    params_player.add(player_a_frame_position_float.set("player_a_frame_float",0,0,1));
    
    params_player.add(player_b_load_movie_b.set("player_b_load",0));
    params_player.add(player_b_file.set("player_b_file",""));
    params_player.add(player_b_path.set("player_b_path",""));
    params_player.add(player_b_frame_position.set("player_b_frame",0,0,1));
    params_player.add(player_b_frame_position_float.set("player_b_frame_float",0,0,1));
    
    params_player.add(player_set_playhead_f.set("set_playhead",0,0,1));
    params_player.add(player_rate.set("player_rate",1,-4,4));
   
    player_a_path.addListener(this, &vplay2::player_a_path_changed);
    player_b_path.addListener(this, &vplay2::player_b_path_changed);

    
    
    player_set_playhead_f.addListener(this, &vplay2::player_set_playhead_f_changed);
    params_player.add(fbo_res_x.set("fbo_res_x",1920,1,4096));
    fbo_res_x.addListener(this, &vplay2::fbo_res_x_changed);
    params_player.add(fbo_res_y.set("fbo_res_y",1080,1,2160));
    fbo_res_y.addListener(this, &vplay2::fbo_res_y_changed);
    
    
    //allocate fbo
    fbo_res_changed = 1;
}

void vplay2::reload_conf()
{
    player_rate_range=player_rate_range.get(); //workaround assign to to itself?
    player_a_path=player_a_path.get();
    player_b_path=player_b_path.get();
    fbo_res_changed=1;
}

//--------------------------------------------------------------


void vplay2::update()
{
    check_loading();
    if(player_a.isLoaded() && player_b.isLoaded())
    {
        if (!player_a_loading && !player_b_loading)
        {
            if(!disable_tick)
            {
                float rev=0;
                if(backward==1)
                {
                     rev=-1;
                }else{
                     rev=1;
                }
                float r = ((player_rate*player_rate_multiplicator)+(external_influence*player_rate_multiplicator))*rev;
                if(r!=computed_rate)
                    {
                        computed_rate=r;
                    }
                frame_index=frame_index+computed_rate;
                player_set_frame(frame_index);
            }

        }

        else
        {
            player_a_loading=0;
            player_b_loading=0;
            player_a_frame_position.setMax(player_a_framecount);
            player_b_frame_position.setMax(player_b_framecount);
            fbo_res_x = player_a.getWidth();
            fbo_res_y = player_a.getHeight();
        }
        if (enable)
        {
            if (computed_rate>=0)
            {
                player_a.update();
            }else{
                player_b.update();
            }
        }
    if (fbo_res_changed)
    {
        fbo.allocate(fbo_res_x, fbo_res_y, GL_RGBA); //-> H V ->var from params
        fbo_res_changed = 0;
    }
}
}
//--------------------------------------------------------------

void vplay2::draw()
{
    if (enable)
    {
        fbo.begin();
        if(player_a.isLoaded()&&player_b.isLoaded())
        {
            if (computed_rate>=0)
            {
                player_a.draw(0, 0, player_a.getWidth(), player_a.getHeight());
            }else{
                player_b.draw(0, 0, player_b.getWidth(), player_b.getHeight());

            }
        }
        fbo.end();

        if(player_a.isLoaded()&&player_b.isLoaded())
         {
            ofRectangle videoRect(0, 0, player_a.getWidth(), player_a.getHeight());
            videoRect.scaleTo(ofGetWindowRect());
            fbo.draw(videoRect.x, videoRect.y, videoRect.width, videoRect.height);
         }
    }

}
//--------------------------------------------------------------

void vplay2::player_a_load(std::string movie)
{
    if (!movie.empty())
    {
        player_a_file=string(ofFilePath::getBaseName(movie));
        player_a_path=string(movie);
        player_a.load(movie);
        player_a_loading=1;
        player_a.update();
        player_a_framecount=player_a.getTotalNumFrames();
        cout<<"player_a_framecount = "<< player_a_framecount<<endl;
    }
}

void vplay2::player_b_load(std::string movie)
{
    if (!movie.empty())
    {
        player_b_file=string(ofFilePath::getBaseName(movie));
        player_b_path=string(movie);
        player_b.load(movie);
        player_b_loading=1;
        player_b_framecount=player_b.getTotalNumFrames();
        player_b.update();
        cout<<"player_b_framecount = "<< player_b_framecount<<endl;

    }
}


//--------------------------------------------------------------

void vplay2::player_set_frame(float f)
{
    player_framecount=(player_a_framecount+player_b_framecount)/2;
    // catch min / max and loop
        if(frame_index<=0)
        {
            frame_index=player_framecount;
        } else if (frame_index>=player_framecount){
            frame_index=0;
        }

        //
        if(old_frame_index!=int(frame_index))
            {
                float pos =(frame_index/player_framecount);
                if(enable)
                {
                    player_a.setPosition(pos);
                    player_a.setFrame(frame_index);
                    player_b.setPosition(1-pos);
                    player_b.setFrame(abs(player_b_framecount-frame_index)); //ici pour inverser?
                }
               
                //cout<<pos<<endl;
                old_frame_index=int(frame_index);
                //
                player_a_frame_position=int(frame_index);
                player_a_frame_position_float=player_a_frame_position/player_a_framecount;
                player_b_frame_position=int(player_b_framecount-frame_index);
                player_b_frame_position_float=player_b_frame_position/player_b_framecount;
            }
}
//--------------------------------------------------------------
void vplay2::direct_drive_f_changed(float &f)
{
    if(direct_drive_b)
    {
        player_rate=f*player_rate_range;
    }
}
//--------------------------------------------------------------

void vplay2::player_rate_range_changed(float &f)
{
    player_rate.setMax(f);
    player_rate.setMin(-f);
}
//--------------------------------------------------------------

void vplay2::player_a_path_changed(string &s)
{
    if (s!=player_a_path_old)
    {
        player_a_load(s);
        player_a_path_old=s;
    }
}
//--------------------------------------------------------------

void vplay2::player_b_path_changed(string &s)
{
    if (s!=player_b_path_old)
    {
        player_b_load(s);
        player_b_path_old=s;
    }
}

void vplay2::framerate_changed(float &f)
{
    ofSetFrameRate(int(f));
}

//--------------------------------------------------------------

void vplay2::fbo_res_x_changed(int &i)
{
    fbo_res_changed = 1;
}

//--------------------------------------------------------------


void vplay2::fbo_res_y_changed(int &i)
{
    fbo_res_changed = 1;
}

//--------------------------------------------------------------

void vplay2::player_set_playhead_f_changed(float &f)
{
    if(int(f*player_framecount)!=frame_index)
    {
        frame_index=int(f*player_framecount);
    }
}

//--------------------------------------------------------------

void vplay2::check_loading()
{
    if(player_a_load_movie_b)
    {
        ofFileDialogResult result = ofSystemLoadDialog("Load file");
        if(result.bSuccess)
        {
          string path = result.getPath();
          // load your file at `path`
            player_a_load(path);
        }
        player_a_load_movie_b=0;
    }
    if(player_b_load_movie_b)
    {
        ofFileDialogResult result = ofSystemLoadDialog("Load file");
        if(result.bSuccess)
        {
          string path = result.getPath();
          // load your file at `path`
            player_b_load(path);
        }
        player_b_load_movie_b=0;
    }
}
