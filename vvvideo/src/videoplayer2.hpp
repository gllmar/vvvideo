//
//  videoplayer.hpp
//  vvhap
//
//  Created by gllm on 2022-02-28.
//

#ifndef videoplayer2_hpp
#define videoplayer2_hpp

#include "ofMain.h"
#include "ofxGui.h"


class vplay2
{
    public:
    void setup(int index);
    void update();
    void draw();
    
    
    ofVideoPlayer player_a;
    bool player_a_enable = 0;
    bool player_a_loading=0;
    int player_a_framecount;
    float player_a_frame_index;
    ofParameter<bool> player_a_load_movie_b;
    ofParameter<string> player_a_file;
    ofParameter<string> player_a_path;
    string player_a_path_old;
    void player_a_path_changed(string &s);
    ofParameter<float> player_a_frame_position;
    ofParameter<float> player_a_frame_position_float;
    void player_a_load(std::string movie);
    
    ofVideoPlayer player_b;
    bool player_b_enable = 0;
    bool player_b_loading=0;
    int player_b_framecount;
    float player_b_frame_index;
    ofParameter<bool> player_b_load_movie_b;
    ofParameter<string> player_b_file;
    ofParameter<string> player_b_path;
    string player_b_path_old;
    void player_b_path_changed(string &s);
    ofParameter<float> player_b_frame_position;
    ofParameter<float> player_b_frame_position_float;
    void player_b_load(std::string movie);

    int player_framecount;
   
    
    float frame_index;
    int old_frame_index;
    bool player_new_frame;
    
    ofxGuiGroup params_player;
    
    ofParameter<bool> enable;
    ofParameter<bool> disable_tick; //
    ofParameter<float> player_rate;
    void player_rate_changed(float &f);
    
    ofParameter<float> player_rate_multiplicator;

    ofParameter<float> external_influence;
    ofParameter<float> computed_rate;
    float old_computed_rate;

    
    ofParameter<float> player_set_playhead_f;
    void player_set_playhead_f_changed(float &f);
    
    ofParameter<float> player_rate_range;
    void player_rate_range_changed(float &f);
    

    
    

    ofParameter<float> framerate;
    ofParameter<bool> backward;
    void framerate_changed(float &f);
    
    ofParameter<bool> direct_drive_b;
    ofParameter<float> direct_drive_f;
    void direct_drive_f_changed(float &f);
    
    

    
    void player_set_frame(float f);
    void reload_conf();

    ofFbo fbo;
    ofParameter<int> fbo_res_x;
    void fbo_res_x_changed(int &i);
    ofParameter<int> fbo_res_y;
    void fbo_res_y_changed(int &i);
    bool fbo_res_changed=0;
    void allocate_fbo(int x, int y);
    
    void check_loading();
    
   

};

#endif /* videoplayer_hpp */

