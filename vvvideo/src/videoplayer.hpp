//
//  videoplayer.hpp
//  vvhap
//
//  Created by gllm on 2022-02-28.
//

#ifndef videoplayer_hpp
#define videoplayer_hpp

#include "ofMain.h"
#include "ofxGui.h"


class vplay
{
    public:
    void setup(int index);
    void update();
    void draw();
    ofVideoPlayer player;
    
    int player_framecount;
    bool player_loading=0;
    float frame_index;
    int old_frame_index;
    bool player_new_frame;
    ofxGuiGroup params_player;
    
    ofParameter<bool> enable;

    ofParameter<bool> is_hap; //todo detect if it is hap, fallback to ofVideoPlayer?
    ofParameter<bool> disable_tick; //
    
    
    
    ofParameter<float> player_rate;
    void player_rate_changed(float &f);
    
    ofParameter<float> player_rate_multiplicator;

    ofParameter<float> external_influence;
    ofParameter<float> computed_rate;
    float old_computed_rate;
    
    ofParameter<float> player_frame_position;
    ofParameter<float> player_frame_position_float;
    
    ofParameter<float> player_set_playhead_f;
    void player_set_playhead_f_changed(float &f);
    
    ofParameter<float> player_rate_range;
    void player_rate_range_changed(float &f);
    
    ofParameter<string> player_file;
    ofParameter<string> player_path;
    string player_path_old;
    void player_path_changed(string &s);
    
    

    ofParameter<float> framerate;
    ofParameter<bool> backward;
    void framerate_changed(float &f);
    
    ofParameter<bool> direct_drive_b;
    ofParameter<float> direct_drive_f;
    void direct_drive_f_changed(float &f);
    
    

    void load(std::string movie);
    void player_set_frame(float f);
    void reload_conf();

    ofFbo fbo;
    ofParameter<int> fbo_res_x;
    void fbo_res_x_changed(int &i);
    ofParameter<int> fbo_res_y;
    void fbo_res_y_changed(int &i);
    bool fbo_res_changed=0;
    void allocate_fbo(int x, int y);
    
    ofParameter<bool> load_movie_path_b;

};

#endif /* videoplayer_hpp */

