#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxOscParameterSync.h"
#include "videoplayer.hpp"
#include "videoplayer2.hpp"
#include "io.hpp"
#include "psync.hpp"

#define VV_BUILD_WITH_SYPHON 1
#ifdef VV_BUILD_WITH_SYPHON
    #include "ofxSyphon.h"
#endif



class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
    ofParameter<bool> get_params_b;
    ofParameter<bool> load_conf;
    ofParameter<bool> save_conf;
        void save_config();
        void load_config();
    string find_rm_string(string input_s, string pattern_s);
    
    ofParameter<bool> videoplayer_ab;
    ofParameter<bool> auto_swap_ab;
    
    void get_params();
    
    psync psync;


    ofxGuiGroup parameters;
    ofxGuiGroup params_system;
    ofxGuiGroup params_player;
    ofxPanel gui;
    
    ofParameter<bool> fullscreen;
    void fullscreen_changed(bool &b);
    ofParameter<float> gui_draw =0;

    
    double time_now;
    double time_old;
    

    float drag_old;
    float drag_delta;
    ofParameter<float> drag_delta_accum;
    double frame_time_old;
    double frame_delta;
    

    
    void time_to_dec();
    ofParameter<float> drag_time_factor;
    void dec_delta_accum(float f);
    
    bool draw_new_frame;

    
    ofParameter<string> hostname;
    ofParameter<bool> hide_mouse;
    void hide_mouse_changed(bool &b);
    
    void check_mouse_gui_state();
    
    ofParameter<float> framerate;
    void framerate_changed(float &f);
    // player
    vplay2 videoplayer_a;
    vplay2 videoplayer_b;

    //io
    osc_input osc_input;
    osc_output osc_output;

    //scroll
    void mouseScrolled(int x, int y, float scrollX, float scrollY );
    
    
    //Syphon
#ifdef VV_BUILD_WITH_SYPHON
    ofxSyphonServer mainOutputSyphonServer;
#endif

};
