#include "ofApp.h"
//
//--------------------------------------------------------------
void ofApp::setup()
{
    //shader

    
    videoplayer_a.setup(1);
    videoplayer_b.setup(2);
    osc_input.setup();
    osc_output.setup();
    
    psync.setup();

    gui.setup("vvvideo");
    parameters.setName("parameters");
    
    params_player.setName("player");
    params_system.setName("system");
    
    params_system.add(hostname.set("hostname",""));
    params_system.add(save_conf.set("save_conf",0));
    params_system.add(load_conf.set("load_conf",0));
    params_system.add(get_params_b.set("get_params",0));
    params_system.add(videoplayer_ab.set("videoplayer_ab",0));
    params_system.add(auto_swap_ab.set("auto_swap_ab",0));

   
//    params_sync.setup("sync");
//    params_sync.add(sync_enable.set("sync_enable", 1));
//    params_sync.add(sync_receive_port.set("sync_receive_port", 16667, 1024, 65535));
//    params_sync.add(sync_send_address.set("sync_send_address", "localhost"));
//    params_sync.add(sync_send_port.set("sync_send_port", 16666, 1024, 65535));
//    params_sync.add(get_params_b.set("get_params",0));
//

    
    params_system.add(fullscreen.set("fullscreen",0));
    params_system.add(hide_mouse.set("hide_mouse",0));
    params_system.add(framerate.set("of_framerate",60,12,300));
    
    
    
//    params_system.add(&params_sync);
    gui.add(&params_system);
    gui.add(&videoplayer_a.params_player);
    gui.add(&videoplayer_b.params_player);
    gui.add(&osc_input.params);
    gui.add(&osc_output.params);
    
    gui.add(&psync.params);
    
    gui.minimizeAll();

    string  my_str = string(ofSystem("hostname"));
    //remove line feed from string
    my_str.erase(remove(my_str.begin(), my_str.end(), '\n'), my_str.end()); 
    //remove ".local" from string
    string to_erase = ".local";
    size_t pos = my_str.find(to_erase);
    if (pos != std::string::npos)
        {
        // If found then erase it from string
        my_str.erase(pos, to_erase.length());
        }
    hostname=my_str;
    cout<<"Hostname :: "<<hostname<<endl;
    gui.loadFromFile(string(hostname)+".json");
    
    psync.init(gui);

    
    fullscreen.addListener(this, &ofApp::fullscreen_changed);
    framerate.addListener(this, &ofApp::framerate_changed);
    hide_mouse.addListener(this, &ofApp::hide_mouse_changed);

    
    
    ofSetVerticalSync(true);
    ofBackground(0);
    get_params();

    osc_input.reset();
    ofSetWindowTitle("vvideo");
    // Syphon
    #ifdef VV_BUILD_WITH_SYPHON
        mainOutputSyphonServer.setName("VVVIDEO");
    #endif

        

}



void ofApp::get_params()
{
    // get  params to broadcast msg
    fullscreen=fullscreen.get();
    ofSleepMillis(2);
    hide_mouse=hide_mouse.get();
    ofSleepMillis(2);
    framerate=framerate.get();
    ofSleepMillis(2);
    videoplayer_a.player_rate=videoplayer_a.player_rate.get();
    ofSleepMillis(2);
    videoplayer_a.player_rate_range=videoplayer_a.player_rate_range.get();
    ofSleepMillis(2);
    videoplayer_a.player_rate_multiplicator=videoplayer_a.player_rate_multiplicator.get();
    ofSleepMillis(2);

}
//--------------------------------------------------------------
void ofApp::update()
{

    psync.update(gui);
        
    //function pour compter le temps
    time_to_dec();
    if (load_conf)
    {
        gui.loadFromFile(string(hostname)+".json");
        get_params();
        load_conf=0;
    }
    if (save_conf)
    {
        gui.saveToFile(string(hostname)+".json");
        save_conf=0;
    }
    if (get_params_b)
    {
        get_params();
        get_params_b=0;
    }


    
    osc_input.update(videoplayer_a.player_rate);//player_rate
    videoplayer_a.external_influence=osc_input.accumulator;
    videoplayer_b.external_influence=osc_input.accumulator;
    osc_output.process_data(videoplayer_a.computed_rate);
    osc_output.process_data(videoplayer_b.computed_rate);


    osc_output.update();
    if(auto_swap_ab)
    {
        videoplayer_ab=!videoplayer_ab;
    }
    if(videoplayer_ab)
    {
        videoplayer_b.enable=1;
        videoplayer_a.enable=0;
    } else {
        videoplayer_b.enable=0;
        videoplayer_a.enable=1;
    }
    
    videoplayer_a.update();
    videoplayer_b.update();
    
    //std::stringstream strm;
    //strm << "fps: " << ofGetFrameRate();
    //ofSetWindowTitle(strm.str());


}

//--------------------------------------------------------------
void ofApp::draw()
{
    videoplayer_a.draw();
    videoplayer_b.draw();
    #ifdef VV_BUILD_WITH_SYPHON
        mainOutputSyphonServer.publishScreen();
    #endif
    if (gui_draw)
    {
        gui.draw();
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    if(key==9) //tab
    {
        gui_draw=!gui_draw;
        check_mouse_gui_state();
    }
    if(key==102) //f
    {
        fullscreen =!fullscreen;
    }
    if(key==109) //m
    {
        hide_mouse =!hide_mouse;
    }
    if(key==98) //m
    {
        videoplayer_ab=!videoplayer_ab;
    }
    if(key==OF_KEY_PAGE_UP)
    {
    osc_output.process_scroll_XY(5, 5);

    }
    if(key==OF_KEY_PAGE_DOWN)
    {
    osc_output.process_scroll_XY(-5, -5);
    }
    if(key==OF_KEY_HOME)
    {
        osc_output.process_send_BTN();
    }
    //cout<<"key: "<<key<<endl;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y )
{

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{
    
    // augmenter 
    // determiner le deltaX
    
    float drag_now;
    drag_now= x/float(ofGetScreenWidth());
    drag_delta=drag_now-drag_old;
    if (drag_delta>0.1 || drag_delta<-0.1)//compensate for new touch elsewhere
    {
        drag_delta=0;
    }else if (drag_delta>0.02|| drag_delta<-0.02) //ceilling
    {
        drag_delta=0.02;
    }
    drag_delta=drag_delta*100;
    drag_delta_accum=drag_delta_accum+drag_delta;
    drag_old=drag_now;
    //cout<<"drag_delta: "<<drag_delta<<endl;
    
    videoplayer_a.direct_drive_f=ofMap(x, 0, ofGetWidth(), -1, 1);
    videoplayer_b.direct_drive_f=ofMap(x, 0, ofGetWidth(), -1, 1);

    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
    if(button==2)
    {
        gui_draw=!gui_draw;
        check_mouse_gui_state();
    }
    if(button==1)
    {
        osc_output.process_scroll_BTN(button, 1);
    }
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{
    if(button==1)
    {
        osc_output.process_scroll_BTN(button, 0);
    }
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y)
{

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y)
{

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg)
{

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo)
{
    vector< string > fileList = dragInfo.files;
    videoplayer_a.player_a_path=(fileList[0]);
    videoplayer_a.player_b_path=(fileList[1]);
    videoplayer_b.player_a_path=(fileList[2]);
    videoplayer_b.player_b_path=(fileList[3]);

}

//--------------------------------------------------------------

void ofApp::framerate_changed(float &f)
{
    ofSetFrameRate(int(f));
}


//--------------------------------------------------------------
void ofApp::time_to_dec()
{
    frame_delta=ofGetElapsedTimeMillis()-frame_time_old;
    frame_time_old=ofGetElapsedTimeMillis();
    float drag_delta_variator=frame_delta*drag_time_factor;
    
    
    if(drag_delta_accum<0.00)
    {
        //negative value, increment
        drag_delta_accum=drag_delta_accum+drag_delta_variator;
        if (drag_delta_accum>0.00)
        {
           drag_delta_accum=0.00;
            //cout<<">0"<<endl;
        }
    }else if (drag_delta_accum>0.00){
        //positive value, decrement
        drag_delta_accum=drag_delta_accum-drag_delta_variator;
        if (drag_delta_accum<0.00)
        {
           drag_delta_accum=0.00;
            //cout<<"<0"<<endl;
        }
        
    } else
    {
        drag_delta_accum=0;
    }
}

//--------------------------------------------------------------

string ofApp::find_rm_string(string input_s, string pattern_s)
{
    std::string t = input_s;
    std::string s = pattern_s;

    std::string::size_type i = t.find(s);
    std::size_t pos = t.find(s);
    t.erase(s.size()-1);//rm last char (\13)
    if(pos !=std::string::npos){
        t.erase(pos,s.length());
        
        return t;
    }else{
        std::cout<<"Substring does not exist in the string\n";
        return t;
    }
    return 0;
}

//--------------------------------------------------------------

void ofApp::hide_mouse_changed(bool &b)
{
    if(hide_mouse)
    {
        ofHideCursor();
    } else {
        ofShowCursor();
    }
}

//--------------------------------------------------------------

void ofApp::check_mouse_gui_state()
{
    if(gui_draw&&hide_mouse){ofShowCursor();}
    if(!gui_draw&&hide_mouse){ofHideCursor();}
}

//--------------------------------------------------------------

void ofApp::fullscreen_changed(bool &b)
{
    ofSetFullscreen(b);
}

//--------------------------------------------------------------

void ofApp::mouseScrolled(int x, int y, float scrollX, float scrollY ){
        // cout<<"mouseScrolled : x "<< x<< ": y " << y <<" scrollX : "<< scrollX << " scrollY : " <<scrollY << endl;
    osc_output.process_scroll_XY(scrollX, scrollY);
    

}
//--------------------------------------------------------------
