//
//  io.cpp
//  vvhap
//
//  Created by gllm on 2022-01-03.
//

#include "io.hpp"

void osc_input::setup()
{
    params.setup("osc_input");
    params.add(input_port.set("input_port",10000,1024,65536));
    params.add(learn.set("learn",0));
    params.add(string_to_match.set("string_to_match", "/encoder/"));
    params.add(data.set("data",0,0,1));
    params.add(input_scaler.set("input_scaler",0.001,0,10));
    params.add(decrement_enable.set("decrement_enable",0));
    params.add(accumulator_max.set("accumulator_max",200,0,10000));
    params.add(accumulator.set("accumulator",0,-1,1));
    params.add(timeout_millis.set("timeout_millis", 200, 0, 25000));
    params.add(decrement_unit.set("decrement_unit", 0.1, 0, 1));
    input_port.addListener(this, &osc_input::input_port_changed);
    connect();
}

void osc_input::connect()
{
    receiver.setup(input_port);
}

void osc_input::update(float player_rate)
{
    while(receiver.hasWaitingMessages())
    {
        ofxOscMessage m;
        receiver.getNextMessage(m);
        if(learn)
        {
            string_to_match=m.getAddress();
            learn=0;
        }
        if(m.getAddress() == string(string_to_match))
        {
            for(size_t i = 0; i < m.getNumArgs(); i++)
            {
                // display the argument - make sure we get the right type
                if(m.getArgType(i) == OFXOSC_TYPE_INT32)
                {
                    data=(m.getArgAsInt32(i));
                    accum(int(data));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT)
                {
                    data=(m.getArgAsFloat(i));
                    accum(int(data));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_STRING)
                {
                    cout<< "unhandled string type " + m.getArgTypeName(i)<<endl;
                }
                else
                {
                    cout<< "unhandled argument type " + m.getArgTypeName(i)<<endl;
                }
            }
        }
    }
    if ((old_time + timeout_millis) > ofGetElapsedTimeMillis())
    {
        decrement=0;
    } else {
        decrement=1;
    }
    if(decrement && decrement_enable)
    {
        //é//float dec_log = accumulator/10;
        float dec_log = 0;

        if(accumulator >player_rate*-2)
        {
            if( abs(accumulator) > decrement_unit)
            {
                accumulator=accumulator-decrement_unit-dec_log;
            } else {
                accumulator=0;
            }
        }
        else
        {
            if( abs(accumulator) > decrement_unit+abs(player_rate))
            {
                accumulator=accumulator+decrement_unit-dec_log;
            } else {
                //accumulator=0;
                accumulator=player_rate*-2;
            }
        }
    }
}

void osc_input::accum(int number)
{

    accumulator += number*input_scaler;
    if(abs(accumulator)>accumulator_max)
    {
        if(accumulator>0){accumulator=accumulator_max;}
        if(accumulator<0){accumulator=-accumulator_max;}
    }
    old_time=ofGetElapsedTimeMillis();

}

void osc_input::input_port_changed(int &i)
{
    connect();
    cout<<"osc listening on port :: " << input_port <<endl;
}

void osc_input::reset()
{
    data=0;
    accumulator=0;
}
//------------------------------------//

void osc_output::setup()
{
    params.setup("osc_output");
    params.add(output_port.set("output_port",10001,1024,65536));
    params.add(output_ip.set("output_ip","localhost"));
    params.add(output_message.set("output_message","/speed/"));
    params.add(scale_factor.set("scale_factor",0.1,0.01,10));
    params.add(output_min.set("output_minimum",-1,-100,100));
    params.add(output_max.set("output_maximum",1,-100,100));
    params.add(output_data.set("output_data",0,-100,100));
    
    output_ip.addListener(this, &osc_output::output_ip_changed);
    output_port.addListener(this, &osc_output::output_port_changed);
    
    
    connect();
}

void osc_output::update()
{
    if(send_osc_flag)
    {
    ofxOscMessage m;
    m.setAddress(output_message);
    m.addFloatArg(output_data);
    sender.sendMessage(m, false);
    old_data=output_data;
    send_osc_flag=0;
    }
}

void osc_output::connect()
{
        sender.setup(output_ip, output_port);
        ofxOscMessage m;
        m.setAddress("/message/");
        m.addFloatArg(1);
        sender.sendMessage(m, false);
}

void osc_output::process_data(float data)
{
    if(data*scale_factor != old_data)
    {
        output_data=ofClamp((data*scale_factor),output_min,output_max);
        old_data=output_data;
        send_osc_flag=1;
    }
}

void osc_output::process_scroll_XY(float scrollX, float scrollY)
{
    ofxOscMessage m;
    m.setAddress("/scrollX");
    m.addFloatArg(scrollX);
    sender.sendMessage(m, false);
    ofxOscMessage n;
    n.setAddress("/scrollY");
    n.addFloatArg(scrollY);
    sender.sendMessage(n, false);
}


void osc_output::output_port_changed(int &i)
{
    connect();
}

void osc_output::output_ip_changed(string &s)
{
    connect();
}

void osc_output::process_scroll_BTN(int btn, int state)
{
    ofxOscMessage m;
    m.setAddress("/scrollBTN");
    m.addFloatArg(float(btn));
    m.addFloatArg(float(state));
    sender.sendMessage(m, false);
}

void osc_output::process_send_BTN()
{
    ofxOscMessage m;
    m.setAddress("/scrollBTN");
    m.addFloatArg(float(1));
    m.addFloatArg(float(0));
    sender.sendMessage(m, false);
    btnState=!btnState;
    ofxOscMessage n;
    n.setAddress("/scrollBTN");
    n.addFloatArg(float(1));
    n.addFloatArg(float(1));
    sender.sendMessage(n, false);
}
