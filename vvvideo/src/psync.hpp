//
//  psync.hpp
//  vvhap
//
//  Created by gllm on 2022-01-10.
//

#ifndef psync_hpp
#define psync_hpp

#include <stdio.h>
#include "ofMain.h"
#include "ofxGui.h"
#include "ofxOscParameterSync.h"



class psync
{
public:
    void setup();
    void init(ofxPanel &gui);
    void update(ofxPanel &gui);
    
    
    ofxOscReceiver osc_sync_init;
    bool sync_initialized =0;
    ofxOscParameterSync sync;
    ofxGuiGroup params;
    ofParameter<bool> sync_enable;
    void sync_enable_changed(bool &b);
    ofParameter<int> sync_receive_port;
    void sync_receive_port_changed(int &i);
    ofParameter<int> sync_send_port;
    void sync_send_port_changed(int &i);
    ofParameter<string> sync_send_address;
    void sync_send_address_changed(string &s);
    void sync_init();
    void sync_param_changed();
    bool sync_param_changed_b=0;
    
    
};


#endif /* psync_hpp */

