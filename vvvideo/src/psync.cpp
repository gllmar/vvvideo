//
//  psync.cpp
//  vvhap
//
//  Created by gllm on 2022-01-10.
//

#include "psync.hpp"

void psync::setup()
{
    params.setup("sync");
    params.add(sync_enable.set("sync_enable", 1));
    params.add(sync_receive_port.set("sync_receive_port", 16667, 1024, 65535));
    params.add(sync_send_address.set("sync_send_address", "localhost"));
    params.add(sync_send_port.set("sync_send_port", 16666, 1024, 65535));
    //
    sync_enable.addListener(this, &psync::sync_enable_changed);
    
}

void psync::init(ofxPanel &gui)
{
    if(sync_enable && !sync_initialized)
    {
        // crash après deux fois
        sync.setup((ofParameterGroup&)gui.getParameter(),sync_receive_port,sync_send_address,sync_send_port);
        sync_initialized=1;
    }
}

void psync::update(ofxPanel &gui)
{
    if (sync_param_changed_b && sync_enable && !sync_initialized)
    {
        init(gui);
    }
    if(sync_enable)
    {
        sync.update();
    }
}

void psync::sync_param_changed()
{
    sync_param_changed_b=1;
}
void psync::sync_enable_changed(bool &b)
{
    if(b)
    {
        sync_param_changed();
    }
}



void psync::sync_send_address_changed(string &s){sync_param_changed();}

void psync::sync_send_port_changed(int &i){sync_param_changed();}

void psync::sync_receive_port_changed(int &i){sync_param_changed();}

