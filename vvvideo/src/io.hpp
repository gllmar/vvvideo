//
//  io.hpp
//  vvhap
//
//  Created by gllm on 2022-01-03.
//

#ifndef io_hpp
#define io_hpp

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxOsc.h"

class osc_input
{
public:
    void setup();
    void connect();
    void update(float player_rate);
    void reset();
    ofxOscReceiver receiver;
    
    ofxGuiGroup params;
    ofParameter<int> input_port;
    void input_port_changed(int &i);
    ofParameter<bool> learn;
    ofParameter<string> string_to_match;
    ofParameter<float> data;
    ofParameter<float> input_scaler;
    ofParameter<bool> decrement_enable;
    ofParameter<float> accumulator_max;
    ofParameter<float> accumulator;
    ofParameter<int> timeout_millis;
    ofParameter<float> decrement_unit;
    bool decrement =0;
    double old_time;
    
    
    void accum(int number);
    
};

class osc_output
{
public:
    void setup();
    void connect();
    void update();
    ofxOscSender sender;
    ofxGuiGroup params;
    ofParameter<float> scale_factor;
    ofParameter<int> output_port;
    void output_port_changed(int &i);
    ofParameter<string> output_ip;
    void output_ip_changed(string &s);
    ofParameter<string> output_message;
    ofParameter<float> output_data;
    ofParameter<float> output_min;
    ofParameter<float> output_max;
    
    void process_data(float data);
    void process_scroll_XY(float scrollX, float scrollY);
    void process_scroll_BTN(int btn, int state);
    void process_send_BTN();
    bool btnState;
    float old_data =1;
    bool send_osc_flag=0;

    
    
};

#endif /* io_hpp */
