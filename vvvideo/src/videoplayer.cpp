//
//  vplay.cpp
//  vvhap
//
//  Created by gllm on 2022-02-28.
//

#include "videoplayer.hpp"

void vplay::setup(int index)
{

    params_player.setName("vplay_"+to_string(index));
    params_player.add(enable.set("enable",1));
    params_player.add(load_movie_path_b.set("load_file",0));
    params_player.add(player_file.set("player_file",""));
    params_player.add(player_path.set("player_path",""));
    params_player.add(disable_tick.set("disable_tick",0));
    params_player.add(direct_drive_b.set("direct_drive_b",0));
    params_player.add(direct_drive_f.set("direct_drive_f",0,-1,1));
    params_player.add(player_frame_position.set("frame",0,0,1));
    params_player.add(player_frame_position_float.set("frame_float",0,0,1));
    params_player.add(player_set_playhead_f.set("set_playhead",0,0,1));
    params_player.add(player_rate.set("player_rate",1,-4,4));
    params_player.add(backward.set("backward",0));
    params_player.add(player_rate_range.set("rate_range",1,0.1,64));
    params_player.add(player_rate_multiplicator.set("rate_multiplicator",2, 0.1, 64));
    params_player.add(computed_rate.set("computed_rate",0,-10,10));
    direct_drive_f.addListener(this, &vplay::direct_drive_f_changed);
    player_rate_range.addListener(this, &vplay::player_rate_range_changed);
    player_path.addListener(this, &vplay::player_path_changed);
    
    player_set_playhead_f.addListener(this, &vplay::player_set_playhead_f_changed);
    params_player.add(fbo_res_x.set("fbo_res_x",1920,1,4096));
    fbo_res_x.addListener(this, &vplay::fbo_res_x_changed);
    params_player.add(fbo_res_y.set("fbo_res_y",1080,1,2160));
    fbo_res_y.addListener(this, &vplay::fbo_res_y_changed);
    
    
    //allocate fbo
    fbo_res_changed = 1;
}

void vplay::reload_conf()
{
   player_rate_range=player_rate_range.get(); //workaround assign to to itself?
   player_path=player_path.get();
   fbo_res_changed=1;
}

//--------------------------------------------------------------


void vplay::update()
{
    if(load_movie_path_b)
    {
        ofFileDialogResult result = ofSystemLoadDialog("Load file");
        if(result.bSuccess)
        {
          string path = result.getPath();
          // load your file at `path`
            load(path);
        }
        load_movie_path_b=0;
    }
    if(player.isLoaded())
    {
        if (!player_loading)
        {
            if(!disable_tick)
            {
                float rev=0;
                if(backward==1)
                {
                     rev=-1;
                }else{
                     rev=1;
                }
                float r = ((player_rate*player_rate_multiplicator)+(external_influence*player_rate_multiplicator))*rev;
                if(r!=computed_rate)
                    {
                        computed_rate=r;
                    }
                frame_index=frame_index+computed_rate;
                player_set_frame(frame_index);
                player_frame_position=int(frame_index);
                player_frame_position_float=player_frame_position/player_framecount;
                
            }

        }

        else
        {
            player_framecount=player.getTotalNumFrames();
            //cout<<player.getTotalNumFrames()<<endl;
            player_loading=0;
            player_frame_position.setMax(player_framecount);
            fbo_res_x = player.getWidth();
            fbo_res_y = player.getHeight();
        }
        if (enable)
        {
            player.update();
        }
    }
    if (fbo_res_changed)
    {
        fbo.allocate(fbo_res_x, fbo_res_y, GL_RGBA); //-> H V ->var from params
        fbo_res_changed = 0;
    }
}
//--------------------------------------------------------------

void vplay::draw()
{
    if (enable)
    {
        fbo.begin();
        if(player.isLoaded())
        {
            player.draw(0, 0, player.getWidth(), player.getHeight());
        }
        fbo.end();

         if(player.isLoaded())
         {
            ofRectangle videoRect(0, 0, player.getWidth(), player.getHeight());
            videoRect.scaleTo(ofGetWindowRect());
            fbo.draw(videoRect.x, videoRect.y, videoRect.width, videoRect.height);
         }
    }

}
//--------------------------------------------------------------

void vplay::load(std::string movie)
{
    if (!movie.empty())
    {
        player_file=string(ofFilePath::getBaseName(movie));
        player_path=string(movie);
        player.load(movie);
        player_loading=1;
    }
}


//--------------------------------------------------------------

void vplay::player_set_frame(float f)
{
    // catch min / max and loop
        if(frame_index<=0)
        {
            frame_index=player_framecount;
        } else if (frame_index>=player_framecount){
            frame_index=0;
        }

        //
        if(old_frame_index!=int(frame_index))
            {
                float pos =(frame_index/player_framecount);
                //player.setPosition(pos);
                player.setFrame(frame_index);
                //cout<<pos<<endl;
                old_frame_index=int(frame_index);
            }
}
//--------------------------------------------------------------
void vplay::direct_drive_f_changed(float &f)
{
    if(direct_drive_b)
    {
        player_rate=f*player_rate_range;
    }
}
//--------------------------------------------------------------

void vplay::player_rate_range_changed(float &f)
{
    player_rate.setMax(f);
    player_rate.setMin(-f);
}
//--------------------------------------------------------------

void vplay::player_path_changed(string &s)
{
    if (s!=player_path_old)
    {
        load(s);
        player_path_old=s;
    }
}
//--------------------------------------------------------------


void vplay::framerate_changed(float &f)
{
    ofSetFrameRate(int(f));
}

void vplay::fbo_res_x_changed(int &i)
{
    fbo_res_changed = 1;
}

void vplay::fbo_res_y_changed(int &i)
{
    fbo_res_changed = 1;
}

void vplay::player_set_playhead_f_changed(float &f)
{
    if(int(f*player_framecount)!=frame_index)
    {
        frame_index=int(f*player_framecount);
    }
}
