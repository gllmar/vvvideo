#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$SCRIPT_DIR"

curl -L https://github.com/openframeworks/openFrameworks/releases/download/0.11.2/of_v0.11.2_osx_release.zip -o of_v0.11.2_osx_release.zip
unzip of_v0.11.2_osx_release.zip
rm of_v0.11.2_osx_release.zip
cd of_v0.11.2_osx_release/addons
git clone https://github.com/astellato/ofxSyphon
